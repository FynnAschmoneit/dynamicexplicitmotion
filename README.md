## dynamicExplicitMotion

Mesh topology algorithm allowing for oscillating surfaces. See details in:

Aschmoneit, F., Jensen, J.H., Saremi, S., Hélix-Nielsen, C. Fluxes of sediment beneath floating silt screens due to density gradients and screen motion, Journal of Waterway, Port, Coastal, and Ocean Engineering, 2020, Vol. 146 (4), doi:10.1061/(ASCE)WW.1943-5460.0000568